import csv
import re
import sys
from dataclasses import dataclass
from io import StringIO
from pathlib import Path
from typing import (
    Dict,
    List,
    Optional,
)

total_races = 7
drop_weeks = 2
keep_weeks = total_races - drop_weeks
scoring = [25, 18, 15, 12, 10, 8, 6, 4, 2, 1]
file_regex = re.compile("^LeagueRace([0-9]+)_(Feature|Heat).csv$")


def points_by_position_0_indexed(position: int) -> int:
    if position < len(scoring):
        return scoring[position]
    else:
        return 0


def finish_order_to_scoring_order(
        finish_order: List[str]
) -> Dict[str, int]:
    """
    Turn list of drivers into dict of driver -> points.
    """
    return {
        driver: points_by_position_0_indexed(index)
        for index, driver in enumerate(finish_order)
    }


def load_result_file(file: Path, class_id: Optional[str] = None) -> List[str]:
    """
    Return list of drivers sorted by finishing order.
    """
    with open(str(file.absolute()), 'r', encoding='utf8', errors='ignore') as f:
        contents = f.read()

    body = contents[contents.find('"Fin Pos"'):]
    lookup = None
    finish_order = []

    # Results are always ordered in finishing position descending, so we do
    # not have to read finishing position here.
    for row in csv.reader(StringIO(body), delimiter=","):
        if lookup is None:
            lookup = {item: index for index, item in enumerate(row)}
        elif class_id is None or row[lookup["Car Class ID"]] == class_id:
            finish_order.append(row[lookup["Name"]])

    return finish_order


@dataclass
class WeekendFiles:
    heat: Path
    feature: Path


def files_by_week(results_dir: Path) -> List[WeekendFiles]:
    # Big assumption here: files must always come in pairs.
    files = [f for f in results_dir.iterdir() if file_regex.match(f.name)]
    heats = sorted([f for f in files if "Heat" in f.name], key=lambda x: x.name)
    features = sorted([f for f in files if "Feature" in f.name], key=lambda x: x.name)

    return [WeekendFiles(heat=h, feature=f) for h, f in zip(heats, features)]


def points_for_week(
        results: WeekendFiles,
        class_id: Optional[str] = None
) -> Dict[str, int]:
    heat_points = finish_order_to_scoring_order(
        load_result_file(results.heat, class_id)
    )
    feature_points = finish_order_to_scoring_order(
        load_result_file(results.feature, class_id)
    )
    drivers = set(list(heat_points.keys()) + list(feature_points.keys()))

    return {
        d: heat_points.get(d, 0) + feature_points.get(d, 0) for d in drivers
    }


@dataclass
class ChampionshipRow:
    driver: str
    total_points: str
    weeks_run: int


def main(results_dir: Path, class_id: Optional[str]):
    week_files = files_by_week(results_dir)
    week_points = [points_for_week(files, class_id) for files in week_files]

    all_drivers = set(d for w in week_points for d in w.keys())
    results = []

    for driver in all_drivers:
        points_list = sorted([w.get(driver, 0) for w in week_points], reverse=True)
        weeks_run = sum(1 if driver in wp else 0 for wp in week_points)
        if weeks_run > keep_weeks:
            points_list = points_list[:keep_weeks]

        results.append(ChampionshipRow(
            driver=driver,
            total_points=sum(points_list),
            weeks_run=weeks_run
        ))

    results.sort(key=lambda x: x.total_points, reverse=True)

    print(f"Championship Standings after week {len(week_files)}:")
    print()

    for row in results:
        print(f"{row.total_points} -- {row.driver} ({row.weeks_run} races)")


if __name__ == "__main__":
    main(results_dir=Path(sys.argv[1]), class_id=sys.argv[2])
